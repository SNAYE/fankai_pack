<br/>
<div align="center">
  <p align="center">
    <img src="assets/Logo_Fankai.png" alt="Logo Fankai-Pack" width="120" height="120">
    <h3 align="center">🌟 Fankai-Pack 🌟</h3>

Regroupement des Packs NFO de @hikano et des différentes versions de l'Utilitaire de @ElPouki

📖 Pour un guide détaillé sur l'utilisation du Pack FanKai, veuillez consulter [notre Wiki](https://gitlab.com/ElPouki/fankai_pack/-/wikis/home).

[Signaler un Bug 🐛](https://github.com/ElPouki/fankai_pack/issues)

</div>

![Contributeurs](https://img.shields.io/gitlab/contributors/ElPouki/fankai_pack?color=dark-green)
![Projet Status](https://www.repostatus.org/badges/latest/active.svg)

## 📚 Table des Matières

- [À propos du Projet](#à-propos-du-projet)
- [Pour Commencer](#pour-commencer)
  - [Prérequis](#prérequis)
  - [Installation](#installation)
- [Utilisation](#utilisation)
- [Auteurs](#auteurs)

## 📖 À propos du Projet

Ce projet a été créé afin de faciliter la reconnaissance des différents Films Kai par nos serveurs médias préférés.

## 🚀 Pour Commencer

### Prérequis

- Un MediaServer (Plex, Jellyfin...) fonctionnel.
- Avoir téléchargé le plugin XBMC disponible dans ce dépôt.
- Du temps.

### 🛠 Installation

Suivez les instructions détaillées dans notre [Wiki](https://gitlab.com/ElPouki/fankai_pack/-/wikis/home) pour une installation et configuration optimales.

## 💡 Utilisation

Découvrez comment tirer le meilleur parti des Packs FanKai et des utilitaires pour une expérience multimédia enrichie en consultant notre [Wiki](https://gitlab.com/ElPouki/fankai_pack/-/wikis/home).

## ✍️ Auteurs

- **ElPouki** - *Initiateur* - [ElPouki](https://gitlab.com/ElPouki)
- **Hikano** - *Initiateur* - [Hikano]()
- **Brazh** - *Création du Wiki* - Un grand merci à Brazh pour son travail considérable !
